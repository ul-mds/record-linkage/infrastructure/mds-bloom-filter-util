package de.ul.mds.pprl.bloom;

import de.ul.mds.pprl.bloom.harden.CLKHardener;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.*;
import java.security.*;
import java.util.List;
import java.util.random.*;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * This class contains convenience functions for use of CLKs in PPRL.
 */
public class CLKUtils {

    private CLKUtils() {}

    /**
     * Throws an {@link IllegalArgumentException} stating that a certain hash algorithm isn't registered.
     *
     * @param alg name of hash algorithm
     * @return formatted exception
     */
    private static IllegalArgumentException newUnregisteredAlgorithmException(String alg) {
        return new IllegalArgumentException(format("%s is not a registered hash algorithm", alg));
    }

    /**
     * Computes the hash digest of a value using the specified hash function.
     *
     * @param alg hash function to use
     * @param value value to compute digest for
     * @return hash digest of value using specified hash function
     * @throws IllegalArgumentException if the hash function is not registered
     * @throws NullPointerException if {@code alg} or {@code value} are {@code null}
     */
    public static byte[] computeHash(String alg, byte[] value) {
        MessageDigest md;

        try {
            md = MessageDigest.getInstance(requireNonNull(alg));
        } catch (NoSuchAlgorithmException e) {
            throw newUnregisteredAlgorithmException(alg);
        }

        return md.digest(requireNonNull(value));
    }

    /**
     * Computes the hash-based message authentication code (HMAC) of a value using the specified hash function.
     *
     * @param alg hash function to use
     * @param value value to compute HMAC for
     * @param key key to use for HMAC
     * @return HMAC of value using specified hash function
     * @throws IllegalArgumentException if the hash function is not registered or the key is invalid
     * @throws NullPointerException if {@code alg}, {@code value} or {@code key} are {@code null}
     */
    public static byte[] computeHmac(String alg, byte[] value, byte[] key) {
        SecretKeySpec sks = new SecretKeySpec(requireNonNull(key), requireNonNull(alg));
        Mac mac;

        try {
            mac = Mac.getInstance(requireNonNull(alg));
        } catch (NoSuchAlgorithmException e) {
            throw newUnregisteredAlgorithmException(alg);
        }

        try {
            mac.init(sks);
        } catch (InvalidKeyException e) {
            throw new RuntimeException("failed to initialize MAC", e);
        }

        return mac.doFinal(requireNonNull(value));
    }

    /**
     * Sets a bit in a CLK.
     * If {@code i} is negative, its bits are flipped.
     * This function then takes the index mod the size of the CLK to determine the bit to set.
     *
     * @param clk CLK to set bit in
     * @param i number to use to compute the index of the bit to set in the CLK
     */
    public static void setBit(CLK clk, int i) {
        if (i < 0) i = ~i;
        clk.set(i % clk.getSize());
    }

    /**
     * Sets bits in a CLK using the double-hashing scheme that was originally proposed by Kirsch and Mitzenmacher.
     *
     * @param clk CLK to set bits in
     * @param k amount of hash values to generate
     * @param h1 first hash value
     * @param h2 second hash value
     */
    public static void doubleHash(CLK clk, int k, int h1, int h2) {
        for (int i = 0; i < k; i++) {
            h1 += h2;
            setBit(clk, h1);
        }
    }

    /**
     * Sets bits in a CLK using the enhanced double-hashing scheme built on top of the original approach by Kirsch and Mitzenmacher.
     * A number is added to every computed index to counteract edge cases of the original double-hashing scheme.
     *
     * @param clk CLK to set bits in
     * @param k amount of hash values to generate
     * @param h1 first hash value
     * @param h2 second hash value
     */
    public static void enhancedDoubleHash(CLK clk, int k, int h1, int h2) {
        for (int i = 0; i < k; i++) {
            int f = i + 1;
            int e = Double.valueOf((Math.pow(f, 3) - f) / 6).intValue();
            int h = h1 + f*h2 + e;

            setBit(clk, h);
        }
    }

    /**
     * Sets bits in a CLK by drawing random numbers from a PRNG, using the hash value as a seed.
     *
     * @param clk CLK to set bits in
     * @param k amount of hash values to generate
     * @param h seed hash value
     * @param rgf instance of {@link RandomGeneratorFactory} to use
     */
    public static void randomHash(CLK clk, int k, int h, RandomGeneratorFactory<?> rgf) {
        RandomGenerator rng = rgf.create(h);

        for (int i = 0; i < k; i++) {
            // no need for setBit() here since we can just use the upper bound of the nextInt() function instead
            clk.set(rng.nextInt(clk.getSize()));
        }
    }

    /**
     * Converts an array of bytes to a 64-bit integer.
     * This is done by taking the first eight bytes (big endian) and converting them to a number of type {@link Long}.
     *
     * @param bytes array of bytes to use
     * @return first eight bytes of array as 64-bit integer
     */
    public static long bytesToLong(byte[] bytes) {
        return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getLong();
    }

    /**
     * Splits a 64-bit integer into a single 32-bit integer.
     * This is done by isolating the first four bytes (big endian) and returning them as an integer.
     *
     * @param l 64-bit integer to use
     * @return first four bytes of long as 32-bit integer
     */
    public static int splitLongToInt(long l) {
        return (int)(l >> 32);
    }

    /**
     * Splits a 64-bit integer into two 32-bit integers.
     * This is done by isolating the first and last four bytes (big endian) and returning them as integers.
     *
     * @param l 64-bit integer to use
     * @return first and last four bytes of long as 32-bit integers
     */
    public static int[] splitLongToInts(long l) {
        return new int[] {(int)(l >> 32), (int)l};
    }

    /**
     * Runs a series of hardeners on a CLK.
     * The hardeners are applied in the order they are listed.
     *
     * @param clk CLK to harden
     * @param hardeners list of hardeners to apply
     * @return processed CLK
     */
    public static CLK chainHardeners(CLK clk, List<CLKHardener> hardeners) {
        requireNonNull(clk);

        for (CLKHardener hardener : requireNonNull(hardeners)) {
            clk = hardener.harden(clk);
        }

        return clk;
    }

    /**
     * Computes the optimal CLK size such that a certain amount of bits can be expected to be set given a certain amount of insertions.
     *
     * @param p percentage of bits that is expected to be set
     * @param n amount of expected insertions into the CLK
     * @return optimal CLK size such that the specified percentage of bits is expected to be set after the expected amount of insertions
     * @throws IllegalArgumentException if the amount of insertions isn't positive, or if the percentage is not within the range of [0,1)
     */
    public static int optimalCLKSize(double p, double n) {
        if (n <= 0) throw new IllegalArgumentException(format("amount of expected insertions must be positive, is %f", n));
        if (p < 0 || p >= 1) throw new IllegalArgumentException(format("percentage of set bits must be in range of [0,1), is %f", p));

        return Double.valueOf(Math.ceil(1 / (1 - Math.pow(p, 1/n)))).intValue();
    }

}
