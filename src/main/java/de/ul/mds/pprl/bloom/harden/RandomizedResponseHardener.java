package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;

import java.util.BitSet;
import java.util.function.Supplier;
import java.util.random.RandomGenerator;

import static java.util.Objects.requireNonNull;

/**
 * Randomized response performs, as the name implies, randomized response on a CLK.
 * Given a probability <i>p</i>, a random number between 0 and 1 is generated for every bit.
 * If the number is lower than or equal to <i>p</i>, the bit is changed.
 * In this case, if the number is less than or higher than <i>p/2</i>, the bit is unset or set respectively.
 * Otherwise, the bit is kept as-is.
 */
public class RandomizedResponseHardener implements CLKHardener {

    /**
     * RNG provider function
     */
    private final Supplier<RandomGenerator> rngSupp;

    /**
     * Bit mutation probability
     */
    private final double probability;

    /**
     * Creates a new randomized response hardener instance.
     *
     * @param rngSupp function that provides a random number generator
     * @param probability probability with which a bit mutation is performed
     * @throws IllegalArgumentException if the probability is not within the range of [0,1]
     * @throws NullPointerException if {@code rngSupp} is {@code null}
     */
    public RandomizedResponseHardener(Supplier<RandomGenerator> rngSupp, double probability) {
        if (probability < 0 || probability > 1) {
            throw new IllegalArgumentException("probability is out of range, must be within [0,1]");
        }

        this.rngSupp = requireNonNull(rngSupp);
        this.probability = probability;
    }

    @Override
    public CLK harden(CLK clk) {
        int m = requireNonNull(clk).getSize();
        BitSet bs = (BitSet) clk.getBitSet().clone();
        RandomGenerator rng = rngSupp.get();

        for (int i = 0; i < m; i++) {
            double d = rng.nextDouble();

            // if p < 1 - prob, then leave bit as-is
            if (d > probability) {
                continue;
            }

            // otherwise, set it to true or false with p/2 probability each
            bs.set(i, d < probability / 2);
        }

        return CLK.wrap(bs, m);
    }

}
