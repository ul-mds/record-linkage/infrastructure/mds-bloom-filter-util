package de.ul.mds.pprl.bloom;

import java.util.*;

import static java.util.Objects.requireNonNull;

public class StringTokenizer {

    private StringTokenizer() {}

    /**
     * Checks that the token size is positive.
     *
     * @param tokenSize token size to check
     * @return parameter that was passed in
     * @throws IllegalArgumentException if the token size is not
     */
    private static int validateTokenSize(int tokenSize) {
        if (tokenSize <= 0) throw new IllegalArgumentException("token size must be positive");

        return tokenSize;
    }

    /**
     * Computes the upper bound of tokens to expect for a specified string with the selected token size.
     *
     * @param value string to compute token count for
     * @param tokenSize size of text tokens
     * @return upper bound for text tokens to expect
     * @throws NullPointerException if the string is {@code null}
     * @throws IllegalArgumentException if the token size is not positive
     */
    public static int estimateTokens(String value, int tokenSize) {
        return estimateTokensInternal(requireNonNull(value), validateTokenSize(tokenSize));
    }

    /**
     * Internal token estimation function.
     * Asserts that all parameters passed their respective sanity checks.
     *
     * @param value string to compute token count for
     * @param tokenSize size of text tokens
     * @return upper bound for text tokens to expect
     */
    private static int estimateTokensInternal(String value, int tokenSize) {
        return value.length() - 1 + tokenSize;
    }

    /**
     * Splits a string up into tokens of set size.
     *
     * @param value string to split
     * @param tokenSize size of text tokens
     * @param padding string to use as padding (ignored if token size is one)
     * @return set of text tokens of specified size
     * @throws IllegalArgumentException if the token size is not positive, or if the padding doesn't have a length of one
     * @throws NullPointerException if {@code value} or {@code padding} are {@code null}
     */
    public static Set<String> tokenize(String value, int tokenSize, String padding) {
        if (padding.length() != 1) {
            throw new IllegalArgumentException("padding must have a length of one");
        }

        // create set and preallocate space to fit the maximum amount of tokens
        var tokens = new HashSet<String>(estimateTokensInternal(requireNonNull(value), validateTokenSize(tokenSize)));
        // generate padding, or use empty string if the token size is one
        String p = tokenSize > 1 ? requireNonNull(padding).repeat(tokenSize - 1) : "";
        String paddedValue = p + value + p;

        for (int i = 0; i < paddedValue.length() - tokenSize + 1; i++) {
            String token = paddedValue.substring(i, i + tokenSize);
            tokens.add(token);
        }

        return tokens;
    }

}
