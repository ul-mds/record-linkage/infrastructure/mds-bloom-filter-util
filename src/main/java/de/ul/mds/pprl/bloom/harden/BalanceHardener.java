package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;

import java.util.BitSet;

import static java.util.Objects.requireNonNull;

/**
 * Balancing extends a CLK with a copy of itself and flipping its bits.
 * This is to ensure that exactly half of all bits are set.
 */
public class BalanceHardener implements CLKHardener {

    @Override
    public CLK harden(CLK clk) {
        int m = requireNonNull(clk).getSize();
        // retrieve a clone of the original bitset, so we can mutate it
        BitSet bs = (BitSet) clk.getBitSet().clone();

        // construct the second half of the clk by setting all bits that are unset in the first half
        for (int i = bs.nextClearBit(0); (i >= 0) && (i < m); i = bs.nextClearBit(i+1)) {
            bs.set(i+m);
        }

        return CLK.wrap(bs, m*2);
    }

}
