package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;

import java.util.BitSet;

import static java.util.Objects.requireNonNull;

/**
 * XOR folding performs a bit-wise XOR on the first half of a CLK with its second half.
 * If the CLK size is odd, it is padded by an extra zero-bit at the end.
 */
public class XORFoldHardener implements CLKHardener {

    @Override
    public CLK harden(CLK clk) {
        int m = requireNonNull(clk).getSize();
        BitSet bs = clk.getBitSet();

        // pad clk if length is odd
        if ((m&1) == 1) m++;

        // halve the clk length
        m /= 2;
        // separate first and second half of bitset
        BitSet bs1 = bs.get(0, m);
        BitSet bs2 = bs.get(m, m*2);
        // perform the folding operation
        bs1.xor(bs2);

        return CLK.wrap(bs1, m);
    }

}
