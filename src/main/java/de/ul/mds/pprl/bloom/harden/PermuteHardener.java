package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;

import java.util.BitSet;
import java.util.function.Supplier;
import java.util.random.RandomGenerator;

import static java.util.Objects.requireNonNull;

/**
 * Permute performs a random permutation of bits in a CLK.
 * The shuffling algorithm used is a simple Fisher-Yates shuffle.
 */
public class PermuteHardener implements CLKHardener {

    /**
     * RNG provider function
     */
    private final Supplier<RandomGenerator> rngSupp;

    /**
     * Creates a new permute hardener.
     *
     * @param rngSupp function that provides a random number generator
     */
    public PermuteHardener(Supplier<RandomGenerator> rngSupp) {
        this.rngSupp = requireNonNull(rngSupp);
    }

    @Override
    public CLK harden(CLK clk) {
        int m = requireNonNull(clk).getSize();
        BitSet bs = (BitSet) clk.getBitSet().clone();
        RandomGenerator rng = rngSupp.get();

        for (int i = m-1; i > 0; i--) {
            int j = rng.nextInt(i);

            // nothing happens if both bits are already the same
            if (bs.get(i) == bs.get(j)) continue;

            // otherwise the bits are flipped
            bs.flip(i);
            bs.flip(j);
        }

        return CLK.wrap(bs, m);
    }

}
