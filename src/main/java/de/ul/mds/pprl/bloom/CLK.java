package de.ul.mds.pprl.bloom;

import java.util.BitSet;
import java.util.Objects;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

public class CLK {

    /**
     * CLK size
     */
    private final int m;

    /**
     * Underlying bitset
     */
    private final BitSet bitSet;

    /**
     * Checks that the CLK size is positive.
     *
     * @param m filter size
     * @return filter size that was passed in
     * @throws IllegalArgumentException if the filter size isn't positive
     */
    private static int checkFilterSize(int m) {
        if (m <= 0) {
            throw new IllegalArgumentException("filter size must be positive");
        }

        return m;
    }

    /**
     * Constructs a new empty CLK of the specified size.
     *
     * @param m CLK size
     * @throws IllegalArgumentException if the size isn't positive
     */
    public CLK(int m) {
        this(new BitSet(checkFilterSize(m)), m);
    }

    /**
     * Constructs a new CLK as a clone of the CLK that is passed into this constructor.
     *
     * @param c CLK to copy
     */
    public CLK(CLK c) {
        this((BitSet) c.bitSet.clone(), c.m);
    }

    /**
     * Internal constructor.
     * Asserts that all sanity checks on the bitset and the CLK size have been performed.
     *
     * @param bs internal bitset
     * @param m CLK size
     */
    private CLK(BitSet bs, int m) {
        this.bitSet = bs;
        this.m = m;
    }

    /**
     * Wraps around an existing {@link BitSet} to use as a CLK.
     * If there are any set bits outside the specified CLK size, they will be unset.
     * This function keeps the reference to the CLK.
     * It will not create a clone of it.
     * If you wish for your bitset to not be mutated by the CLK, create a clone of the bitset before calling this function.
     *
     * @param bs bitset to wrap into a CLK
     * @param m CLK size
     * @return bitset as CLK
     * @throws IllegalArgumentException if the CLK size isn't positive
     * @throws NullPointerException if the bitset is {@code null}
     */
    public static CLK wrap(BitSet bs, int m) {
        CLK clk = new CLK(requireNonNull(bs), checkFilterSize(m));

        // clear all bits that are out of range
        for (int i = bs.nextSetBit(m); i >= 0; i = bs.nextSetBit(i+1)) {
            bs.clear(i);
        }

        return clk;
    }

    /**
     * Returns the size of the CLK in bits.
     *
     * @return Bloom filter size in bits
     */
    public int getSize() {
        return m;
    }

    /**
     * Returns the {@link BitSet} in use by this CLK.
     * <b>WARNING!</b> This {@link BitSet} is mutable.
     * Any operations performed on the return value of this function call will be reflected in this CLK.
     *
     * @return {@link BitSet} in use by this CLK
     */
    public BitSet getBitSet() {
        return bitSet;
    }

    /**
     * Sets a bit in this CLK. The value of the bit at the specified position is set from 0 to 1.
     *
     * @param i position of the bit to set
     * @throws IllegalArgumentException if the bit position is not in range of [0,m)
     */
    public void set(int i) {
        if (i < 0 || i >= m) {
            throw new IllegalArgumentException(format("index out of bounds, must be in range of 0<=i<%d", m));
        }

        bitSet.set(i);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CLK clk = (CLK) o;
        return m == clk.m && bitSet.equals(clk.bitSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(m, bitSet);
    }

    @Override
    public String toString() {
        return "CLK{" +
                "m=" + m +
                ", bitSet=" + bitSet +
                '}';
    }

}
