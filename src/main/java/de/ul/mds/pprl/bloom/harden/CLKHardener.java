package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;

public interface CLKHardener {

    /**
     * Performs a hardening operation on a CLK.
     * In the implementation, the hardener must never mutate the original CLK and must always return a new CLK.
     *
     * @param clk CLK to harden
     * @return Hardened CLK
     */
    CLK harden(CLK clk);

}
