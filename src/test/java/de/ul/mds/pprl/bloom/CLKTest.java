package de.ul.mds.pprl.bloom;

import org.junit.jupiter.api.Test;

import java.util.BitSet;

import static org.junit.jupiter.api.Assertions.*;

public class CLKTest {

    @Test
    public void testWrap() {
        CLK clk = CLK.wrap(BitSet.valueOf(new byte[] {(byte) 0b11111111}), 4);
        BitSet bs = clk.getBitSet();

        for (int i = 0; i < 4; i++) {
            assertTrue(bs.get(i));
        }

        for (int i = 4; i < 8; i++) {
            assertFalse(bs.get(i));
        }
    }

    @Test
    public void testWrapThrowsNonPositive() {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> CLK.wrap(new BitSet(), 0));

        assertEquals("filter size must be positive", ex.getMessage());
    }

    @Test
    public void testNewThrowsNonPositive() {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> new CLK(0));

        assertEquals("filter size must be positive", ex.getMessage());
    }

    @Test
    public void testSetThrowsTooHigh() {
        CLK clk = new CLK(4);

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> clk.set(4));

        assertEquals("index out of bounds, must be in range of 0<=i<4", ex.getMessage());
    }

    @Test
    public void testSetThrowsTooLow() {
        CLK clk = new CLK(4);

        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> clk.set(-1));

        assertEquals("index out of bounds, must be in range of 0<=i<4", ex.getMessage());
    }

}
