package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;
import org.junit.jupiter.api.Test;

import java.util.BitSet;

import static org.junit.jupiter.api.Assertions.*;

public class BalanceHardenerTest {

    @Test
    public void testBalanceHardener() {
        CLK clk = CLK.wrap(BitSet.valueOf(new byte[] {0b1010}), 4);
        CLK hardenedClk = new BalanceHardener().harden(clk);

        assertEquals(CLK.wrap(BitSet.valueOf(new byte[] {(byte)0b01011010}), 8), hardenedClk);
    }

    @Test
    public void testThrowNonNull() {
        assertThrows(NullPointerException.class, () -> new BalanceHardener().harden(null));
    }

}
