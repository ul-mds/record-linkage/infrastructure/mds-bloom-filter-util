package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;
import org.junit.jupiter.api.Test;

import java.util.BitSet;
import java.util.function.Supplier;
import java.util.random.*;

import static org.junit.jupiter.api.Assertions.*;

public class PermuteHardenerTest {

    @Test
    public void testPermuteHardener() {
        CLK clk = CLK.wrap(BitSet.valueOf(new byte[] {(byte)0b10101010}), 8);
        CLK hardenedCLK = new PermuteHardener(() -> RandomGeneratorFactory.getDefault().create(123L)).harden(clk);

        assertNotEquals(clk, hardenedCLK);
        assertEquals(clk.getSize(), hardenedCLK.getSize());
    }

    @Test
    public void testPermuteHardenerSameSeed() {
        RandomGeneratorFactory<RandomGenerator> genFactory = RandomGeneratorFactory.getDefault();

        Supplier<RandomGenerator> rng = () -> genFactory.create(123L);

        CLK clk = CLK.wrap(BitSet.valueOf(new byte[] {(byte)0b10101010}), 8);
        CLK hardenedCLK1 = new PermuteHardener(rng).harden(clk);
        CLK hardenedCLK2 = new PermuteHardener(rng).harden(clk);

        assertEquals(hardenedCLK1, hardenedCLK2);
    }

    @Test
    public void testPermuteHardenerSameCLKSameInstance() {
        var hardener = new PermuteHardener(() -> RandomGeneratorFactory.getDefault().create(123L));

        CLK clk1 = CLK.wrap(BitSet.valueOf(new byte[] {(byte)0b10101010}), 8);
        CLK clk2 = new CLK(clk1);

        CLK hardenedCLK1 = hardener.harden(clk1);
        CLK hardenedCLK2 = hardener.harden(clk2);

        assertEquals(hardenedCLK1, hardenedCLK2);
    }

    @Test
    public void testThrowNotNull() {
        assertThrows(NullPointerException.class, () -> new PermuteHardener(RandomGenerator::getDefault).harden(null));
    }

    @Test
    public void testThrowRngNull() {
        assertThrows(NullPointerException.class, () -> new PermuteHardener(null));
    }

}
