package de.ul.mds.pprl.bloom;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static de.ul.mds.pprl.bloom.StringTokenizer.*;
import static org.junit.jupiter.api.Assertions.*;

public class StringTokenizerTest {

    @Test
    public void testTokenize() {
        assertEquals(Set.of("f", "o", "b", "a", "r"), tokenize("foobar", 1, "_"));
        assertEquals(Set.of("_f", "fo", "oo", "ob", "ba", "ar", "r_"), tokenize("foobar", 2, "_"));
        assertEquals(Set.of("##f", "#fo", "foo", "oob", "oba", "bar", "ar#", "r##"), tokenize("foobar", 3, "#"));
        assertThrows(IllegalArgumentException.class, () -> tokenize("foobar", 0, "_"));
        assertThrows(IllegalArgumentException.class, () -> tokenize("foobar", 2, "##"));
        assertThrows(NullPointerException.class, () -> tokenize(null, 2, "_"));
        assertThrows(NullPointerException.class, () -> tokenize("foobar", 2, null));
    }

    @Test
    public void testEstimateTokens() {
        assertEquals(7, estimateTokens("foobar", 2));
        assertEquals(8, estimateTokens("foobar", 3));
        assertEquals(8, estimateTokens("fooobar", 2));
        assertThrows(IllegalArgumentException.class, () -> estimateTokens("foobar", 0));
        assertThrows(NullPointerException.class, () -> estimateTokens(null, 2));
    }

}
