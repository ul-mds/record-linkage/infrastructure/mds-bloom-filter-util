package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;
import org.junit.jupiter.api.Test;

import java.util.BitSet;

import static org.junit.jupiter.api.Assertions.*;

public class XORFoldHardenerTest {

    @Test
    public void testXORFoldHardener() {
        CLK clk = CLK.wrap(BitSet.valueOf(new byte[] {(byte) 0b11111111, (byte) 0b10100000}), 16);
        CLK hardenedCLK = new XORFoldHardener().harden(clk);

        assertEquals(CLK.wrap(BitSet.valueOf(new byte[] {(byte) 0b11111111 ^ (byte) 0b10100000}), 8), hardenedCLK);
    }

    @Test
    public void testXORFoldHardenerPadded() {
        // this time m == 15, therefore the MSB from the first byte should be trimmed off
        CLK clk = CLK.wrap(BitSet.valueOf(new byte[] {(byte) 0b11111111, (byte) 0b10100000}), 15);
        CLK hardenedCLK = new XORFoldHardener().harden(clk);

        assertEquals(CLK.wrap(BitSet.valueOf(new byte[] {(byte) 0b01111111 ^ (byte) 0b10100000}), 8), hardenedCLK);
    }

    @Test
    public void testThrowNotNull() {
        assertThrows(NullPointerException.class, () -> new XORFoldHardener().harden(null));
    }

}
