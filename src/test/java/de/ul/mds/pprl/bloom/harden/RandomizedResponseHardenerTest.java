package de.ul.mds.pprl.bloom.harden;

import de.ul.mds.pprl.bloom.CLK;
import org.junit.jupiter.api.Test;

import java.util.BitSet;
import java.util.random.*;

import static org.junit.jupiter.api.Assertions.*;

public class RandomizedResponseHardenerTest {

    private static CLK randomCLK(int m) {
        byte[] bytes = new byte[(m / 8) + 1];
        RandomGeneratorFactory.getDefault().create(0L).nextBytes(bytes);
        return CLK.wrap(BitSet.valueOf(bytes), m);
    }

    @Test
    public void testRandomizedResponseHardener() {
        CLK clk = randomCLK(320);
        CLK hardenedClk = new RandomizedResponseHardener(() -> RandomGeneratorFactory.getDefault().create(123L), .5d).harden(clk);

        assertNotEquals(clk, hardenedClk);
        assertEquals(clk.getSize(), hardenedClk.getSize());
    }

    @Test
    public void testRandomizedResponseHardenerSameSeed() {
        CLK clk = randomCLK(480);
        CLK hardenedClk1 = new RandomizedResponseHardener(() -> RandomGeneratorFactory.getDefault().create(123L), .5d).harden(clk);
        CLK hardenedClk2 = new RandomizedResponseHardener(() -> RandomGeneratorFactory.getDefault().create(123L), .5d).harden(clk);

        assertEquals(hardenedClk1, hardenedClk2);
    }

    @Test
    public void testRandomizedResponseHardenerSameSeedDifferentProb() {
        CLK clk = randomCLK(480);
        CLK hardenedClk1 = new RandomizedResponseHardener(() -> RandomGeneratorFactory.getDefault().create(123L), .5d).harden(clk);
        CLK hardenedClk2 = new RandomizedResponseHardener(() -> RandomGeneratorFactory.getDefault().create(123L), .25d).harden(clk);

        assertNotEquals(hardenedClk1, hardenedClk2);
    }

    @Test
    public void testThrowNotNull() {
        assertThrows(NullPointerException.class, () -> new RandomizedResponseHardener(RandomGenerator::getDefault, .5d).harden(null));
    }

    @Test
    public void testThrowRngNull() {
        assertThrows(NullPointerException.class, () -> new RandomizedResponseHardener(null, .5d));
    }

    @Test
    public void testThrowProbTooHigh() {
        assertThrows(IllegalArgumentException.class, () -> new RandomizedResponseHardener(RandomGenerator::getDefault, 1.01d));
    }

    @Test
    public void testThrowProbTooLow() {
        assertThrows(IllegalArgumentException.class, () -> new RandomizedResponseHardener(RandomGenerator::getDefault, -0.1d));
    }

    @Test
    public void testRandomizedResponseHardenerSameCLKSameInstance() {
        var hardener = new RandomizedResponseHardener(() -> RandomGeneratorFactory.getDefault().create(123L), .5d);

        CLK clk1 = CLK.wrap(BitSet.valueOf(new byte[] {(byte)0b10101010}), 8);
        CLK clk2 = new CLK(clk1);

        CLK hardenedCLK1 = hardener.harden(clk1);
        CLK hardenedCLK2 = hardener.harden(clk2);

        assertEquals(hardenedCLK1, hardenedCLK2);
    }

}
