# Bloom filter utilities

This library provides an implementation of cryptographic long keys (CLKs) and utility functions to perform PPRL using Bloom filter based masking techniques.
A good starting point is the following snippet.
Check the library documentation for more details.

```java
import de.hs_mittweida.dbs.utils.bloom.*;
import de.ul.mds.pprl.bloom.*;
import de.ul.mds.pprl.bloom.harden.*;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.random.RandomGeneratorFactory;

import static de.ul.mds.pprl.bloom.CLKUtils.*;

class BloomFilterTest {

    public static void main(String[] args) {
        // create new clk with 256 bits
        CLK clk = new CLK(256);
        // create value and HMAC key
        byte[] value = "foobar".getBytes(StandardCharsets.UTF_8);
        byte[] key = "secret".getBytes(StandardCharsets.UTF_8);
        // compute the hash digest
        byte[] digest = computeHmac("HmacSHA256", value, key);
        // compute hash values
        int[] h = splitLongToInts(bytesToLong(digest));
        // set bits in the clk using the enhanced double hashing scheme
        // k=5, so five bit positions will be set using the provided hash values
        enhancedDoubleHash(clk, 5, h[0], h[1]);

        // create balance hardener
        var balancer = new BalanceHardener();
        // create permute hardener, using 123 as the starting seed for every clk
        var permuter = new PermuteHardener(() -> RandomGeneratorFactory.getDefault().create(123L));
        // perform hardening operations
        CLK hardenedClk = chainHardeners(clk, List.of(balancer, permuter));
        
        // print the resulting clk out
        System.out.printf("CLK=%s%n", Base64.getUrlEncoder().encodeToString(hardenedClk.getBitSet().toByteArray()));
    }

}
```